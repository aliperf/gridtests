#!/bin/bash

set -o pipefail

function per() { printf "\033[31m$1\033[m\n" >&2; }
function pok() { printf "\033[32m$1\033[m\n" >&2; }
function banner() { echo ; echo ==================== $1 ==================== ; }

if [[ ! $ALIEN_PROC_ID && ! $1 ]]; then
   per "Please give a job name"
   exit 1
fi

# General job configuration
MY_USER=aliperf #$( (alien_whoami || true) 2> /dev/null | xargs echo )
if [[ ! $MY_USER ]]; then
  per "Problems retrieving current AliEn user. Did you run alien-token-init?"
  exit 1
fi
MY_HOMEDIR="/alice/cern.ch/user/${MY_USER:0:1}/${MY_USER}"
MY_BINDIR="$MY_HOMEDIR/bin"
MY_JOBPREFIX="$MY_HOMEDIR/selfjobs"
MY_JOBNAME=${1:-job}
MY_JOBNAMEDATE="${MY_JOBNAME}-$(date -u +%Y%m%d-%H%M%S)"
MY_JOBWORKDIR="$MY_JOBPREFIX/${MY_JOBNAMEDATE}"  # ISO-8601 UTC
MY_JOBWORKDIR_TMP="${MY_JOBWORKDIR}_TMP"  # for temporary uploads

# TODO validate MY_JOBNAME

pok "Your job's working directory will be $MY_JOBWORKDIR"
pok "Set the job name by running $0 <jobname>"

# 
# Submitter code
#
if [[ ! $ALIEN_PROC_ID ]]; then
  # We are not on a worker node: assuming client --> test if alien is there?
  which alien.py
  # check exit code
  if [[ ! "$?" == "0"  ]]; then
    XJALIEN_LATEST=`find /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/xjalienfs -type f -printf "%f\n" | tail -n1`
    banner "Loading xjalien package $XJALIEN_LATEST"
    eval "$(/cvmfs/alice.cern.ch/bin/alienv printenv xjalienfs::"$XJALIEN_LATEST")"
  fi

  # Create workdir, and work there
  cd "$(dirname "$0")"
  THIS_SCRIPT="$PWD/$(basename "$0")"
  mkdir -p work/$(basename "$MY_JOBWORKDIR")
  cd work/$(basename "$MY_JOBWORKDIR")

  # Generate JDL
  cat > "${MY_JOBNAMEDATE}.jdl" <<EOF
Executable = "${MY_BINDIR}/${MY_JOBNAMEDATE}.sh";
OutputDir = "${MY_JOBWORKDIR}";
Output = {
  "*.log,log.txt,*time@disk=2"
};
Requirements = member(other.GridPartitions,"multicore_8");
MemorySize = "60GB";
TTL=80000;
EOF
#
#Requirements = member(other.GridPartitions,"multicore_8") && (!member(other.GridPartitions,"SC_Cineca"));

  pok "Local working directory is $PWD"

  pok "Preparing job \"$MY_JOBNAMEDATE\""
  (
    alien.py rmdir "$MY_JOBWORKDIR" || true                                   # remove existing job dir
    alien.py mkdir "${MY_JOBWORKDIR_TMP}" || true
    alien.py mkdir "$MY_BINDIR" || true                                       # create bindir
    alien.py mkdir "$MY_JOBPREFIX" || true                                    # create job output prefix
    alien.py mkdir jdl || true
    alien.py rm "$MY_BINDIR/${MY_JOBNAMEDATE}.sh" || true                     # remove current job script
    alien.py cp "${PWD}/${MY_JOBNAMEDATE}.jdl" alien://${MY_HOMEDIR}/jdl/${MY_JOBNAMEDATE}.jdl@ALICE::CERN::EOS || true  # copy the jdl
    alien.py cp "$THIS_SCRIPT" alien://${MY_BINDIR}/${MY_JOBNAMEDATE}.sh@ALICE::CERN::EOS || true  # copy current job script to AliEn
  ) &> alienlog.txt

  pok "Submitting job \"${MY_JOBNAMEDATE}\" from $PWD"
  (
    alien.py submit jdl/${MY_JOBNAMEDATE}.jdl || true
  ) &>> alienlog.txt

  MY_JOBID=$( (grep 'Your new job ID is' alienlog.txt | grep -oE '[0-9]+' || true) | sort -n | tail -n1)
  if [[ $MY_JOBID ]]; then
    pok "OK, display progress on https://alimonitor.cern.ch/agent/jobs/details.jsp?pid=$MY_JOBID"
  else
    per "Job submission failed: error log follows"
    cat alienlog.txt
  fi

  exit 0
fi

####################################################################################################
# The following part is executed on the worker node
####################################################################################################

# All is redirected to log.txt but kept on stdout as well
if [[ $ALIEN_PROC_ID ]]; then
  exec &> >(tee -a log.txt)
fi


notify_mattermost() {
   text=$1
   WEBHOOK=https://mattermost.web.cern.ch/hooks/15h4aii35jgs3yidzdozd64gfy
   COMMAND="curl --insecure -X POST -H 'Content-type: application/json' --data '{\"text\":\""${text}"\"}' "${WEBHOOK}
   eval "${COMMAND}" &> /dev/null
}


function copyToGRIDWORKDIR() {
  # check if we have a GRID token
  alien.py token-info &> /dev/null
  if [[ "$?" == "0" && $GRIDMODE ]]; then
    alien.py cp -f ${1} alien://${MY_JOBWORKDIR_TMP}/${1}@ALICE::CERN::EOS
  fi
}

# ----------- START WITH UTILITY FUNCTIONS ----------------------------

child_pid_list=
# finds out all the (recursive) child process starting from a parent
# output includes the parent
# output is saved in child_pid_list
childprocs() {
  local parent=$1
  if [ "$parent" ] ; then
    child_pid_list="$child_pid_list $parent"
    for childpid in $(pgrep -P ${parent}); do
      childprocs $childpid
    done;
  fi
}

CPUBURNERPID=""
startcpuburner() {
  if [[ ${CPUBURNER}!="0" ]]; then
    cpuburn -n ${CPUBURNER} -u 0 &> /dev/null &
    CPUBURNERPID=$!
  fi
}

stopcpuburner() {
  if [[ ! "$CPUBURNERPID" == "" ]]; then
    kill -9 ${CPUBURNERPID}
  fi
}

# accumulate return codes
RC_ACUM=0
taskwrapper() {
  # A simple task wrapper launching a DPL workflow in the background 
  # and checking the output for exceptions. If exceptions are found,
  # all participating processes will be sent a termination signal.
  # The rational behind this function is to be able to determine failing 
  # conditions early and prevent longtime hanging executables 
  # (until DPL offers signal handling and automatic shutdown)

  local logfile=$1
  shift 1
  local command="$*"

  notify_mattermost "Starting task ${logfile} for PROC ${ALIEN_PROC_ID}"

  # launch the actual command in the background
  echo "Launching task: ${command} &> $logfile &"
  command="TIME=\"#walltime %e\" ${O2_ROOT}/share/scripts/monitor-mem.sh /usr/bin/time --output=${logfile}_time '${command}'"
  eval ${command} &> $logfile &

  # THE NEXT PART IS THE SUPERVISION PART
  # get the PID
  PID=$!

  while [ 1 ]; do
    # We don't like to see critical problems in the log file.

    # We need to grep on multitude of things:
    # - all sorts of exceptions (may need to fine-tune)  
    # - segmentation violation
    # - there was a crash
    pattern="-e \"xception\"                        \
             -e \"segmentation violation\"          \
             -e \"error while setting up workflow\" \
             -e \"There was a crash.\""
      
    grepcommand="grep -H ${pattern} $logfile >> encountered_exceptions_list 2>/dev/null"
    eval ${grepcommand}
    
    grepcommand="grep -h --count ${pattern} $logfile 2>/dev/null"
    # using eval here since otherwise the pattern is translated to a
    # a weirdly quoted stringlist
    RC=$(eval ${grepcommand})
    
    # if we see an exception we will bring down the DPL workflow
    # after having given it some chance to shut-down itself
    # basically --> send kill to all children
    if [ "$RC" != "" -a "$RC" != "0" ]; then
      echo "Detected critical problem in logfile $logfile"
      notify_mattermost "Problem detected in ${logfile} ALIEN_PROC_ID"
      sleep 2
      copyToGRIDWORKDIR ${logfile}

      # query processes still alive
      child_pid_list=
      childprocs ${PID}
      for p in $child_pid_list; do
        echo "killing child $p"
        kill $p
      done      

      RC_ACUM=$((RC_ACUM+1))
      return 1
    fi

    # check if command returned which may bring us out of the loop
    ps -p $PID > /dev/null
    [ $? == 1 ] && break

    # sleep for some time
    sleep 60

    # continuously send progress reports
    copyToGRIDWORKDIR ${logfile}
  done

  # wait for PID and fetch return code
  # ?? should directly exit here?
  wait $PID

  copyToGRIDWORKDIR ${logfile}
  # return code
  RC=$?
  RC_ACUM=$((RC_ACUM+RC))
  [ ! "${RC} -eq 0" ] && echo "command ${command} had nonzero exit code ${RC}"

  return ${RC}
}


notify_mattermost "Starting job"

banner "Environment"
env

banner "OS detection"
lsb_release -a || true
cat /etc/os-release || true
cat /etc/redhat-release || true

banner "CVMFS nightlies test"
/cvmfs/alice.cern.ch/bin/alienv q | grep nightly | tail -n10

# O2_PACKAGE_LATEST="nightly-20200627-1"
O2_PACKAGE_LATEST=`find /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/O2 -type f -printf "%f\n" | tail -n1`
XJALIEN_LATEST=`find /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/xjalienfs -type f -printf "%f\n" | tail -n1`
banner "Loading O2 package $O2_PACKAGE_LATEST"
eval "$(/cvmfs/alice.cern.ch/bin/alienv printenv O2::"$O2_PACKAGE_LATEST",xjalienfs::"$XJALIEN_LATEST")"

banner "Running workflow"

host=`hostname`
ldd `which o2-sim` > ldd.log
o2exec=`which o2-sim`
notify_mattermost "Starting sim executable ${o2exec}"

P=${ALIEN_PROC_ID}
cat /proc/cpuinfo > cpuinfo.log 
copyToGRIDWORKDIR cpuinfo.log
cat /proc/meminfo > meminfo.log
copyToGRIDWORKDIR meminfo.log

notify_mattermost "Fetching background files for $ALIEN_PROC_ID on ${host}"

NEVENTS=5

# We might want to simulate running in heavily loaded environments where
# other processes are present. In this case, we spawn some cpuburner 
# tasks.
startcpuburner

# alien.py 'cp -glob *.root /alice/cern.ch/user/a/aliperf/cachedPbPb/ file:./' 
taskwrapper sim.log o2-sim -n ${NEVENTS} --configKeyValue "Diamond.width[2]=6." -g pythia8hi -e TGeant3 -j 8 --seed 1

echo "Running digitization for $intRate kHz interaction rate"
gloOpt="-b --run"
taskwrapper tpcdigi.log o2-sim-digitizer-workflow $gloOpt -n ${NEVENTS} --no-IPC --onlyDet TPC --interactionRate 50000 --tpc-lanes 8 --disable-mc
echo "Return status of digitization: $?"

taskwrapper trddigi.log o2-sim-digitizer-workflow $gloOpt -n ${NEVENTS} --onlyDet TRD --interactionRate 50000 --configKeyValues "TRDSimParams.digithreads=10"
echo "Return status of digitization: $?"

taskwrapper restdigi.log o2-sim-digitizer-workflow $gloOpt -n ${NEVENTS} --skipDet TRD,TPC --interactionRate 50000
echo "Return status of digitization: $?"

taskwrapper tpcreco.log o2-tpc-reco-workflow $gloOpt --no-IPC --disable-mc --tpc-digit-reader \"--infile tpcdigits.root\" --input-type digits --output-type clusters,tracks  --tpc-track-writer \"--treename events --track-branch-name Tracks --trackmc-branch-name TracksMCTruth\" --tracker-options \"threads=1\"
echo "Return status of tpcreco: $?"

stopcpuburner

ls -al > allfiles
notify_mattermost "Ending job $ALIEN_PROC_ID"

exit 0
